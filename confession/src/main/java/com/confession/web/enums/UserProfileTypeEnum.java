package com.confession.web.enums;

public enum UserProfileTypeEnum {
	ROLE_USER("ROLE_USER"),
	ROLE_SUPER_ADMIN("ROLE_SUPER_ADMIN"),
	ROLE_GOLDUSER("ROLE_GOLDUSER");
     
    String userProfileType;
     
    private UserProfileTypeEnum(String userProfileType){
        this.userProfileType = userProfileType;
    }
     
    public String getUserProfileType(){
        return userProfileType;
    }
     
}
