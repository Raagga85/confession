package com.confession.web.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 * Override AccessDeniedHandler class of spring security. Redirect to our accessDenied page instead of login page.
 * 
 * @since 21-05-2015
 * @author haritkumar
 */
public class AssistAccessDeniedHandler implements AccessDeniedHandler {

    private String errorPage;

    /**
     * Setter for error page.
     * 
     * @param errorPage String
     */
    public void setErrorPage(final String errorPage) {
        this.errorPage = errorPage;
    }

    /**
     * Redirect to our accessDenied page instead of login page.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param accessDeniedException AccessDeniedException
     * @throws IOException IOException
     * @throws ServletException ServletException
     */
    @Override
    public void handle(final HttpServletRequest request, final HttpServletResponse response, final AccessDeniedException accessDeniedException) throws IOException,
            ServletException {
        String redirectUrl = request.getContextPath() + errorPage;
        redirectUrl = response.encodeRedirectURL(redirectUrl);

        response.sendRedirect(redirectUrl);

    }

}