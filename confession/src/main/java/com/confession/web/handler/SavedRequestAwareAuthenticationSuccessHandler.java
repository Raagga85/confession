/**
 *  Copyright 2015 Mutation Labs . All Rights Reserved.
 *  Mutation Labs PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.confession.web.handler;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

import com.confession.web.Constant.RedirectConstant;
import com.confession.web.enums.UserProfileTypeEnum;

/**
 * @version 1.0, Aug 12, 2015
 * @author haritkumar
 */
public class SavedRequestAwareAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private RequestCache     requestCache     = new HttpSessionRequestCache();
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) throws ServletException,
            IOException {
        SavedRequest savedRequest = requestCache.getRequest(request, response);

        if (savedRequest == null) {
            super.onAuthenticationSuccess(request, response, authentication);

            return;
        }
        String targetUrlParameter = getTargetUrlParameter();
        if (isAlwaysUseDefaultTargetUrl() || targetUrlParameter != null && StringUtils.hasText(request.getParameter(targetUrlParameter))) {
            requestCache.removeRequest(request, response);
            super.onAuthenticationSuccess(request, response, authentication);

            return;
        }
        //handle(request, response, authentication);
        clearAuthenticationAttributes(request);

        // Use the DefaultSavedRequest URL
        String targetUrl = savedRequest.getRedirectUrl();
        logger.debug("Redirecting to DefaultSavedRequest Url: " + targetUrl);
        getRedirectStrategy().sendRedirect(request, response, targetUrl);
    }

    /**
     * This method is used to handle authentication request.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param authentication Authentication
     * @throws IOException IOException
     */
    @Override
    protected void handle(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) throws IOException {
        final String targetUrl = determineTargetUrl(authentication, request, response);

        if (response.isCommitted()) {
            logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
            return;
        }

        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    /**
     * Builds the target URL according to the logic defined in the main class Javadoc.
     * 
     * @param authentication Authentication
     * @return String target URL
     */
    protected String determineTargetUrl(final Authentication authentication, final HttpServletRequest request, final HttpServletResponse response) {
        String url = RedirectConstant.LOGIN;
        boolean isUser = false;
        boolean isSuperAdmin = false;
        
        final Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
       
        for (final GrantedAuthority grantedAuthority : authorities) {
            
        	if (grantedAuthority.getAuthority().equals(UserProfileTypeEnum.ROLE_SUPER_ADMIN.toString())) {
        		isSuperAdmin = true;
                break;
            }
        	if (grantedAuthority.getAuthority().equals(UserProfileTypeEnum.ROLE_USER.toString())) {
            	isUser = true;
                break;
            }
        }

        if (isUser) {
            url = RedirectConstant.USER_DASHBOARD;
        }  
        if(isSuperAdmin){
        	url = RedirectConstant.SUPER_ADMIN_DASHBOARD;
        }
       
        return url;

    }

    /**
     * Setter for RedirectStrategy.
     * 
     * @param redirectStrategy RedirectStrategy
     */
    @Override
    public void setRedirectStrategy(final RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }

    /**
     * Getter for RedirectStrategy.
     * 
     * @return RedirectStrategy
     */
    @Override
    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }

    public void setRequestCache(final RequestCache requestCache) {
        this.requestCache = requestCache;
    }
}
