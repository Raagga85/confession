////////////////////////////////////////////////////////////////////////////////
// $$Id: $$
// $$Change: $$
// $$DateTime: $$
// $$Author: $$
//
// Copyright (c) 2006 - 2016 FORCAM GmbH. All rights reserved.
////////////////////////////////////////////////////////////////////////////////
package com.confession.web.util;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;


@SpringBootApplication
@ComponentScan(basePackages = { "com.confession.web" })
@EnableMongoRepositories(basePackages = "com.confession.web.util")
@EnableWebSecurity
public class SpringBootClass {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(SpringBootClass.class, args);
    }

}
