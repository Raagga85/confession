package com.confession.web.actuator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

//import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.confession.web.Constant.ServerConstant;
import com.confession.web.entity.Role;
import com.confession.web.entity.WebUser;
import com.confession.web.enums.UserProfileTypeEnum;
import com.confession.web.service.UserService;

@Component
public class HealthCheck implements HealthIndicator {

	private static final Logger logger = Logger.getLogger(HealthCheck.class);

	@Autowired
	private UserService appUserService;
	
	/*@Autowired
	private BCryptPasswordEncoder encoder;*/

	HashMap<String, String> messageMap;

	Role roleP;
	WebUser appUserP;

	@Override
	public Health health() {
		messageMap = new HashMap<String, String>();
		messageMap.put("Health", "OK");

		int dbErrorCode = createSuperAdmin();

		if (dbErrorCode != 200) {
			return Health.down().withDetail("Error Code", messageMap).build();
		}
		return Health.up().withDetail("data", messageMap).build();
	}

	public void createUserProfile() {
		try {
			Role userProfile = new Role();
			userProfile.setActive(ServerConstant.ACTIVE);
			//userProfile.setCraetedDate(new LocalDateTime());
			userProfile.setLastModifiedDate(new LocalDateTime());
			
			userProfile.setName(UserProfileTypeEnum.ROLE_SUPER_ADMIN.name());

			//roleP = appUserService.saveUserProfile(userProfile);

			messageMap.put("USER_PROFILE", "CREATED");
		} catch (Exception e) {
			//logger.error("HealthCheck --> createUserProfile -->" + ExceptionUtils.getStackTrace(e));
			//messageMap.put("EXCEPTION", "HealthCheck --> createUserProfile -->" + ExceptionUtils.getStackTrace(e));
		}
	}

	public Integer createSuperAdmin() {
		try {
			if (null != appUserService.getUserByEmail(ServerConstant.email)) {
				logger.info("Super Admin Already Exist In DB -- Hence Not Adding");
				messageMap.put("AdminUser", "ALREADY EXIST");
			} else {
				logger.info("Super Admin Not Exist In DB -- Hence Adding : " + ServerConstant.email);
				WebUser appUser = new WebUser();

				appUser.setEmail(ServerConstant.email);
				
				appUser.setPhone(ServerConstant.primaryContact);
				//String p=encoder.encode("admin");
				appUser.setPassword("admin");
				appUser.setFirstName(ServerConstant.fName);
				appUser.setLastName(ServerConstant.lName);
				appUser.setSignIn(false);
				appUser.setAccountNonExpired(true);
				appUser.setAccountNonLocked(true);
				appUser.setCredentialsNonLocked(true);
				appUser.setEnabled(true);
				appUser.setCredentialsNonExpired(true);
				appUser.setPhoneVerified(true);
				//appUser.setCraetedDate(new LocalDateTime());
				appUser.setLastModifiedDate(new LocalDateTime());
				
				Role role = appUserService.getUserProfileByRoleName(UserProfileTypeEnum.ROLE_SUPER_ADMIN.name());
				Set<Role> roles = new HashSet<Role>();
				if (null != role) {
					roles.add(role);
					appUser.setRoles(roles);
				} else {
					createUserProfile();
					roles.add(roleP);
					appUser.setRoles(roles);
				}

				appUserP = appUserService.saveUser(appUser);

				// Updation User
				
				//appUserService.updateUserProfile(roleP);
				messageMap.put("AdminUser", "CREATED");
			}
		} catch (Exception e) {
			//logger.error("HealthCheck --> createSuperAdmin -->" + ExceptionUtils.getStackTrace(e));
			//messageMap.put("EXCEPTION", "HealthCheck --> createSuperAdmin -->" + ExceptionUtils.getStackTrace(e));
		}
		return 200;
	}

}
