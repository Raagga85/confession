package com.confession.web.service;

import java.util.List;

import com.confession.web.entity.Role;
import com.confession.web.entity.WebUser;

public interface UserService {

	
	WebUser saveUser(WebUser laboAssistUser) throws Exception;
	Role getUserProfileByRoleName(String roleName) throws Exception;
	//Role saveUserProfile(Role userProfile) throws Exception;
	WebUser getUserByEmail(String email) throws Exception;
	//Role updateUserProfile(Role userProfile) throws Exception;
	WebUser updateUser(WebUser user) throws Exception;
	List<WebUser> getAllUser() throws Exception;
	WebUser getUserById(String id) throws Exception;
}
