package com.confession.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
 
/**
 * Spring Web MVC Security Java Config Demo Project
 * Configures authentication and authorization for the application.
 *
 * @author www.codejava.net
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
     
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
       /* auth
            .inMemoryAuthentication()
                .withUser("admin").password("nimda").roles("ADMIN");*/
    	
    	auth.userDetailsService(customUserDetailsService);
    }
     
    @Override
    protected void configure(HttpSecurity http) throws Exception {
   
     http.authorizeRequests()
        .antMatchers("/").permitAll()
        .antMatchers("/u/**").access("hasAnyRole('ROLE_USER','ROLE_GOLDUSER')")
        .antMatchers("/sa/**").access("hasRole('ROLE_SUPER_ADMIN')")
        .antMatchers("/co/**").access("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_USER')")
        .antMatchers("/gu/**").access("hasRole('ROLE_GOLDUSER')")
        .antMatchers("/open/login").authenticated()
        .and().formLogin().defaultSuccessUrl("/open/login");
     
     
    	
    	/*http
		.authorizeRequests()                                                                1
			.antMatchers("/resources/**", "/signup", "/about").permitAll()                  2
			.antMatchers("/admin/**").hasRole("ADMIN")                                      3
			.antMatchers("/db/**").access("hasRole('ADMIN') and hasRole('DBA')")            4
			.anyRequest().authenticated()                                                   5
			.and()
		// ...
		.formLogin();*/
    	
    	/*http
		.authorizeRequests()
			.anyRequest().authenticated()
			.and()
		.formLogin()
			.loginPage("/login") 
			.permitAll();    */
       
      http.csrf().disable();
    }   
}
