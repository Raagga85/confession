package com.confession.web.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.confession.web.custom.annotation.CascadeSaveMongoEventListener;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;


@Configuration
@EnableWebMvc
@EnableMongoRepositories(basePackages = "com.confession.web.util")
public class SpringConf extends AbstractMongoConfiguration {
	
	
    @Override
    protected String getDatabaseName() {
       return "CONFESSIONDB";
    }

    @Override
    @Bean
    public Mongo mongo() throws Exception {
        MongoClient client = new MongoClient("localhost");
        client.setWriteConcern(WriteConcern.SAFE);
        return client;
    }

    @Override
    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), getDatabaseName());
    }
    
    @Bean
    public MongoTemplate mongoTemplate(MongoDbFactory mongoDbFactory) throws Exception {
        return new MongoTemplate(mongoDbFactory);
}
    
    /*@Bean
    public UserCascadeSaveMongoEventListener userCascadingMongoEventListener() {
        return new UserCascadeSaveMongoEventListener();
    }*/

    @Bean
    public CascadeSaveMongoEventListener cascadingMongoEventListener() {
        return new CascadeSaveMongoEventListener();
    }

  /* @Override
    public CustomConversions customConversions() {
        converters.add(new UserWriterConverter());
        return new CustomConversions(converters);
    }*/

  /*  @Bean
    public GridFsTemplate gridFsTemplate() throws Exception {
        return new GridFsTemplate(mongoDbFactory(), mappingMongoConverter());
    }*/
    
    /*@Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean emf() {
        LocalContainerEntityManagerFactoryBean emf =
        emf.setDataSource(dataSource);
        emf.setPackagesToScan(
            new String[] {"your.package"});
        emf.setJpaVendorAdapter(
            new HibernateJpaVendorAdapter());
        return emf;
    }
    
    @Bean(name = "jpaTx")
    public PlatformTransactionManager transactionManagerJPA() throws NamingException {
      JpaTransactionManager txManager = new JpaTransactionManager(new LocalContainerEntityManagerFactoryBean().getObject());

      return txManager;
    }*/
    
   /* @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) throws Exception {
        return new JpaTransactionManager(entityManagerFactory);
}*/
    
   /* @Bean
    public ViewResolver getViewResolver(){
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }*/

    /* @Autowired
     public MongoTemplate mongoTemplate;*/

   /* @Bean
     public UserService userService() {
         return new UserServiceImpl();
    }
    
    @Bean
    public UserConvertor userConverter() {
        return new UserConvertor();
   }
*/
}