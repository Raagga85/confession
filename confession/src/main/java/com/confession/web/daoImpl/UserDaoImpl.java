package com.confession.web.daoImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.confession.web.dao.UserDao;
import com.confession.web.entity.Role;
import com.confession.web.entity.WebUser;
import com.confession.web.util.IUserRepository;

@Repository
public class UserDaoImpl implements UserDao {
	 @Autowired
	    private IUserRepository userRepository;
	
	@Override
	public WebUser saveUser(WebUser user) throws Exception {
		try{
			 userRepository.save(user);
		} catch(Exception e){
			
			throw new Exception(e.getMessage());
		}
		return user;
	}
	/*@Override
	public Role saveUserProfile(Role role) throws Exception {
			try{
				userRepository.save(role);
			} catch(Exception e){
				//logger.error("AppUserDaoImpl --> saveUserProfile -->" +  ExceptionUtils.getStackTrace(e));
				throw new Exception(e.getMessage());
			}
			return role;
	}*/
	@Override

	public Role getUserProfileByRoleName(String roleName) throws Exception {
		Role roleP = null;
		try{
			
		    List<Role> userProfiles = userRepository.getUserProfileByRoleName(roleName);
		    if(null != userProfiles && userProfiles.size() > 0){
		    	roleP = userProfiles.get(0);
		    }
		} catch(Exception e){
			//logger.error("AppUserDaoImpl --> getUserProfileByRoleName -->" +  ExceptionUtils.getStackTrace(e));
			throw new Exception(e.getMessage());
		}
		return roleP;
	}
	@Override
	public WebUser getUserByEmail(String email) throws Exception{
		WebUser appUserP = null;
		try{
			
		    List<WebUser> appUsers = userRepository.getUserByEmail(email);
		    if(null != appUsers && appUsers.size() > 0){
		    	appUserP = appUsers.get(0);
		    }
		} catch(Exception e){
			//logger.error("AppUserDaoImpl --> getAppUserByUsername -->" +  ExceptionUtils.getStackTrace(e));
			throw new Exception(e.getMessage());
		}
		return appUserP;
	}

	@Override
	public WebUser updateUser(WebUser user) throws Exception {
		try{
			WebUser existingUser=userRepository.findOne(user.getEmail());
			existingUser.setRoles(user.getRoles());
			user=userRepository.save(existingUser);
		} catch(Exception e){
			//logger.error("AppUserDaoImpl --> updateUserProfile -->" +  ExceptionUtils.getStackTrace(e));
			throw new Exception(e.getMessage());
		}
		return user;
	}

	

	@Override
	public List<WebUser> getAllUser() throws Exception {
		
		return userRepository.findAll();
	}

	@Override
	public WebUser getUserByid(String id) throws Exception {
		
		return userRepository.findOne(id.toString());
	}
}
