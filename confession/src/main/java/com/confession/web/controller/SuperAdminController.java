package com.confession.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.confession.web.convertor.UserConvertor;
import com.confession.web.dto.WebUserDTO;
import com.confession.web.entity.WebUser;
import com.confession.web.service.UserService;

@Controller
public class SuperAdminController extends BaseController{
	
	@Autowired
	private UserConvertor userConvertor;
	
	@Autowired
	private UserService service;
	
	@RequestMapping(value = "/sa/dashboard", method = RequestMethod.GET)
	public String main(final HttpServletRequest request, final Model model) {
		 getUserInfo(request).getWebUser();
		try {
			
			//model.addAttribute("allPost", postService.getAllPost() );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "/sa/dashboard";
	}
	@RequestMapping(value = "/sa/userCU", method = RequestMethod.GET)
	public String userCU(@ModelAttribute WebUserDTO webUserDTO,final HttpServletRequest request, final Model model,@RequestParam("action") String action, @RequestParam(value = "token", required = false) String token) {
		 getUserInfo(request).getWebUser();
		try {
			if (action.equalsIgnoreCase("u")) {
				webUserDTO = userConvertor.webUserDtoToEntity(webUserDTO, service.getUserById(token));
			}
			
			//model.addAttribute("allPost", postService.getAllPost() );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "/sa/userCU";
	}
	@RequestMapping(value = "/sa/saveUser", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute WebUserDTO webUserDTO,final HttpServletRequest request, final Model model) {
		 getUserInfo(request).getWebUser();
		try {
			
			if (webUserDTO.getAction().equalsIgnoreCase("c")){
			WebUser user=userConvertor.adminUserDtoToEntity(webUserDTO);
			service.saveUser(user);
			model.addAttribute("msg", getMessage("User Added Successfully"));
		}else{
			//updating client info
			service.updateUser(userConvertor.adminUserDtoToEntity(webUserDTO));
			model.addAttribute("msg", getMessage("User Updated Successfully"));
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "/sa/userCU";
	}
	@RequestMapping(value = "/sa/listUser", method = RequestMethod.GET)
	public String userList(@ModelAttribute WebUserDTO webUserDTO,final HttpServletRequest request, final Model model) {
		 getUserInfo(request).getWebUser();
		try {
			
			model.addAttribute("usersList", service.getAllUser() );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "/sa/userList";
	}

}
