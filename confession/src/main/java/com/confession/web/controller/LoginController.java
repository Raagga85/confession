package com.confession.web.controller;

import java.util.Collection;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.confession.web.Constant.RedirectConstant;
import com.confession.web.Constant.ResponseMappingConstant;
import com.confession.web.convertor.UserConvertor;
import com.confession.web.dto.WebUserDTO;
import com.confession.web.entity.WebUser;
import com.confession.web.enums.UserProfileTypeEnum;
import com.confession.web.service.UserService;


@Controller
public class LoginController {
	@Autowired
	private MessageSource resource;
	
	/*@Autowired
	private UserService service;*/
	/*@Autowired
	private UserConvertor userConvertor;*/
	
	@RequestMapping(value = "open/login", method = RequestMethod.GET)
    public String login(@ModelAttribute WebUserDTO webUserDTO,@RequestParam(value = "login_error", required = false) final String error, @RequestParam(value = "logout", required = false) final String logout,
            final HttpServletRequest request, Model model) {
    	
    	 //log.debug("Login  Page");

         System.out.println(getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));  

    	 if (error != null) {
             model.addAttribute("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
         }

         if (logout != null) {
             model.addAttribute("msg", "You are now logged out !");
             request.getSession().invalidate();
         }

        return isRememberMeAuthenticated(request);

    }
	private String getErrorMessage(final HttpServletRequest request, final String key) {
        final Exception exception = (Exception) request.getSession().getAttribute(key);
        String error = "";
        if (exception instanceof BadCredentialsException) {
            error = getMessage("msg.InvalidCredentials");
        } else if (exception instanceof LockedException) {
            error = "Your account is locked, Please contact MailoSMS.in";
        } else {
            error = getMessage("msg.InvalidCredentials");
        }

        return error;
    }
	public final String getMessage(final String code) {

		final Locale locale = LocaleContextHolder.getLocale();
		try {
			return resource.getMessage(code, null, locale);
		} catch (final NoSuchMessageException e) {
			//log.error(e.getMessage());
			return code;
		}
	}

	
	private String isRememberMeAuthenticated(HttpServletRequest request) {
    	boolean isUser = false;
        boolean isSuperAdmin = false;
        boolean isGoldUser = false;
        
        HttpSession session = request.getSession(true);
	    SecurityContextImpl  securityContextImpl  = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
        if (securityContextImpl == null) {
            return ResponseMappingConstant.LOGIN;
        } else {
        	final Collection<? extends GrantedAuthority> authorities = securityContextImpl.getAuthentication().getAuthorities();
            for (final GrantedAuthority grantedAuthority : authorities) {
                
            	if (grantedAuthority.getAuthority().equals(UserProfileTypeEnum.ROLE_SUPER_ADMIN.toString())) {
            		isSuperAdmin = true;
                    break;
                }
            	else if (grantedAuthority.getAuthority().equals(UserProfileTypeEnum.ROLE_USER.toString())) {
            		isUser = true;
                    break;
                }
            	else if (grantedAuthority.getAuthority().equals(UserProfileTypeEnum.ROLE_GOLDUSER.toString())) {
            		isUser = true;
                    break;
                }
            	
        }
      

            if (isUser) {
                return "redirect:"+RedirectConstant.USER_DASHBOARD;
            }  
            else if(isSuperAdmin){
            	return "redirect:"+RedirectConstant.SUPER_ADMIN_DASHBOARD;
            }else if(isGoldUser){
            	return "redirect:"+RedirectConstant.USER_DASHBOARD;
            }
            else{
            	return "redirect:"+ResponseMappingConstant.LOGIN;
            }

        }
    }

}
