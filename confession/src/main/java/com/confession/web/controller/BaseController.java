package com.confession.web.controller;

import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextImpl;

import com.confession.web.model.LoginUser;


public class BaseController {

	static final Logger log = LoggerFactory.getLogger(BaseController.class);

	@Autowired
	private MessageSource resource;

	@Autowired
	ServletContext servletContext;

	/**
	 * This method is used to get message from message property file using key
	 * and defaultCode.
	 * 
	 * @param code
	 *            of message
	 * @param defaultCode
	 *            default code of message
	 * @return String message
	 */
	public final String getMessage(final String code, final String defaultCode) {

		final Locale locale = LocaleContextHolder.getLocale();
		try {
			return resource.getMessage(code, null, locale);
		} catch (final NoSuchMessageException e) {
			log.error(e.getMessage());
			return defaultCode;
		}
	}

	/**
	 * This method is used to get message from message property file using key.
	 * 
	 * @param code
	 *            of message
	 * @return String message
	 */
	public final String getMessage(final String code) {

		final Locale locale = LocaleContextHolder.getLocale();
		try {
			return resource.getMessage(code, null, locale);
		} catch (final NoSuchMessageException e) {
			log.error(e.getMessage());
			return code;
		}
	}

	/**
	 * This method is used to get message from message property file using key
	 * and defaultCode.
	 * 
	 * @param code
	 *            of message
	 * @param args
	 *            Object[] having arguments pass to the message in property file
	 * @return String message
	 */
	public final String getMessageWithArgs(final String code, final Object[] args) {

		final Locale locale = LocaleContextHolder.getLocale();
		try {
			return resource.getMessage(code, args, locale);
		} catch (final NoSuchMessageException e) {
			log.error(e.getMessage());
			return code;
		}
	}

	/**
	 * This method is used to get userInfo currently logged in the application.
	 * 
	 * @return UserInfo object having details of user logged in
	 */
	public final LoginUser getUserInfo(HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		SecurityContextImpl securityContextImpl = (SecurityContextImpl) session.getAttribute("SPRING_SECURITY_CONTEXT");
		final Authentication auth = securityContextImpl.getAuthentication();
		final LoginUser user = (LoginUser) auth.getPrincipal();
		return user;
	}
}
