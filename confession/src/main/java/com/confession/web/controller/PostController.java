package com.confession.web.controller;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.confession.web.Constant.ServerConstant;
import com.confession.web.entity.Comment;
import com.confession.web.entity.Dislikes;
import com.confession.web.entity.Likes;
import com.confession.web.entity.Post;
import com.confession.web.entity.WebUser;
import com.confession.web.service.PostService;

@Controller
public class PostController extends BaseController{
	
	@Autowired
	private PostService postService;
	
	@RequestMapping(value = "/u/post", method = RequestMethod.POST)
	public @ResponseBody Post  post(@ModelAttribute Post post,final HttpServletRequest request, final Model model) {
		WebUser appUser = getUserInfo(request).getWebUser();
		Post posts=new Post();
		try {
			//post.setLastModifiedDate(new LocalDateTime());
			posts.setLikeCount("0");
			posts.setDislikeCount("0");
			posts.setActive(ServerConstant.ACTIVE);
			
			
			posts.setDescription(post.getDescription());
			posts.setComment(post.getComment());
			posts.setCreatedDate(new LocalDateTime());
			posts.setLastModifiedDate(new LocalDateTime());
			posts.setUserId(appUser.getId());
			
			postService.savePost(posts);
			model.addAttribute("allPost", postService.getAllActivePost() );
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return posts;
	}
	@RequestMapping(value = "/u/upost", method = RequestMethod.POST)
	public @ResponseBody String  upost(@ModelAttribute Post post,final HttpServletRequest request, final Model model) {
		WebUser appUser = getUserInfo(request).getWebUser();
		//post.setWebUser(appUser);
		try {
			Post postp=postService.getPostById(post.getId());
			postp.setLastModifiedDate(new LocalDateTime());
			postp.setDescription(post.getDescription());
			postp.setCategory(post.getCategory());
			postp.setActive(ServerConstant.ACTIVE);
			
			postService.updatePost(postp,ServerConstant.ACTIVE);
			//model.addAttribute("allPost", postService.getAllPost() );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "dddddddd";
	}
	@RequestMapping(value = "/u/dpost", method = RequestMethod.POST)
	public @ResponseBody String  dpost(@ModelAttribute Post post,final HttpServletRequest request, final Model model) {
		WebUser appUser = getUserInfo(request).getWebUser();
		//post.setWebUser(appUser);
		try {
			Post postp=postService.getPostById(post.getId());
			
			
			postp.setLastModifiedDate(new LocalDateTime());
			postp.setActive(ServerConstant.INACTIVE);
			
			
			
			postService.updatePost(postp,ServerConstant.INACTIVE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "red";
		
		}
	
	@RequestMapping(value = "/u/comment", method = RequestMethod.POST)
	 public @ResponseBody String  comment(@ModelAttribute Comment comment,final HttpServletRequest request, final Model model) {
	  WebUser appUser = getUserInfo(request).getWebUser();
	  
	  
	  try {
	   Post postp=postService.getPostById(comment.getId());
	   
	   Comment comments=new Comment();
	   comments.setUserId(appUser.getId());
	   comments.setDescription(comment.getDescription());
	   comments.setCount("1");
	   comments.setCreatedDate(new LocalDateTime());
	   comments.setLastModifiedDate(new LocalDateTime());
	   comments.setActive("A");
	   Set<Comment> commentSet= new HashSet<Comment>();
	   commentSet.add(comments);
	   if(null!=postp.getComment()){
	   postp.getComment().add(comments);
	   } else{
	    postp.setComment(commentSet);
	   }
	   
	   postService.savePost(postp);
	  } catch (Exception e) {
	   // TODO Auto-generated catch block
	   e.printStackTrace();
	  }
	  return "red";

	 }
	@RequestMapping(value = "/u/like", method = RequestMethod.POST)
	public @ResponseBody String like(final HttpServletRequest request, final Model model,@RequestParam(value="Status") String Status) {
		WebUser appUser = getUserInfo(request).getWebUser();
		
		boolean alreadyLiked=false;
		try {
			Post postp= postService.getPostById(Status);
			if(null!=postp.getLike()){
			for(Likes like:postp.getLike()){
				if(like.getUserId().equals(appUser.getId())){
				alreadyLiked=true;	
				}
			}
			}
			if(!alreadyLiked){
			//Post postp=postService.getPostById(Status);
			
			/*if(postp.getLiked()!=null){
				postp.setLiked(postp.getLiked()+1);
			} else {
				postp.setLiked("1");
			}*/
			//postService.updatePost(postp);
			Likes like=new Likes();
			//like.setCount("1");
			//like.setCreatedDate(new LocalDateTime());
			//like.setLastModifiedDate(new LocalDateTime());
			like.setUserId(appUser.getId());
			Set<Likes> newLike= new HashSet<Likes>();
			newLike.add(like);
			if(null!=postp && null!=postp.getLike()){
				postp.getLike().add(like);
			} else{
				postp.setLike(newLike);
			}
			
			postp.setLikeCount(new Integer(postp.getLike().size()).toString());
			postService.savePost(postp);
			}
			else{
				return "failure";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "success";
	}
	
	
	@RequestMapping(value = "/u/dislike", method = RequestMethod.POST)
	public @ResponseBody String dislike(final HttpServletRequest request, final Model model,@RequestParam(value="Status") String Status) {
		WebUser appUser = getUserInfo(request).getWebUser();
		
		System.out.println(Status);
		boolean alreadyDisLiked=false;
		try {
			
			Post postdisLikes= postService.getPostById(Status);
		if(null!=postdisLikes.getDislike()){
		for(Dislikes dislike:postdisLikes.getDislike()){
			if(dislike.getUserId().equals(appUser.getId())){
			alreadyDisLiked=true;	
			}
		}
		}
		if(!alreadyDisLiked){
			Post postp=postService.getPostById(Status);
			
			/*if(postp.getDislike()!=null){
				postp.setLiked(postp.getLiked()+1);
			} else {
				postp.setLiked("1");
			}*/
			//postService.updatePost(postp);
			Dislikes dislike=new Dislikes();
			dislike.setCount("1");
			//dislike.setCreatedDate(new LocalDateTime());
			//dislike.setLastModifiedDate(new LocalDateTime());
			dislike.setUserId(appUser.getId());
			Set<Dislikes> newdisLike= new HashSet<Dislikes>();
			newdisLike.add(dislike);
			if(null!=postp && null!=postp.getDislike()){
				postp.getDislike().add(dislike);
			} else{
				postp.setDislike(newdisLike);
			}
			
			postp.setDislikeCount(new Integer(postp.getDislike().size()).toString());
			postService.savePost(postp);
			}
			else{
				return "failure";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "success";
	}

}
