package com.confession.web.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.confession.web.entity.WebUser;


public class LoginUser extends User{
	
	private static final long serialVersionUID = 5357170917120891699L;

	private final WebUser webUser;
	
	public LoginUser(String username, String password, boolean enabled, boolean accountNonExpired,boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities, WebUser webUserP) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.webUser = webUserP;
	}

	public WebUser getWebUser() {
		return webUser;
	}

}

