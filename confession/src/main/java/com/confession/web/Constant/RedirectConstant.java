package com.confession.web.Constant;

public class RedirectConstant {
	public static final String SUPER_ADMIN_DASHBOARD = "/sa/dashboard";
	public static final String USER_DASHBOARD = "/u/dashboard";
	
	public static final String LOGIN = "/open/login";
	
}
