package com.confession.web.entity;


import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

public class BaseEntity {

	private String version;

	private LocalDateTime createdDate;

	

	private LocalDateTime lastModifiedDate;

	

	private String active;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}


	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	


}
