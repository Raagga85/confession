$(".error").focus();

$(".toupper").bind('keyup', function (e) {
    if (e.which >= 97 && e.which <= 122) {
        var newKey = e.which - 32;
        e.keyCode = newKey;
        e.charCode = newKey;
    }

    $(".toupper").val(($(".toupper").val()).toUpperCase());
});


$(".numericonly").keypress(function (e) {
	var node = $(this);
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    	node.attr("placeholder","Numeric only");
        return false;
    }
  });

$(".numericonlycontact").keypress(function (e) {
	var node = $(this);
    if (e.which != 13 && e.which != 0 && (e.which < 48 || e.which > 57)) {
    	node.attr("placeholder","Numeric only");
        return false;
    }
  });


$(".alphaonly").keypress(function (e) {
	var node = $(this);
    if (e.which != 32 && e.which != 8 && e.which != 0 && (e.which < 65 || e.which > 90)  && (e.which < 97 || e.which > 122)) {
    	node.attr("placeholder","Alphabet only");
        return false;
    }
  });


$(document).keypress(function (e) {
    if (e.which == 13) {
        $('#formId').submit();
    }
});


if($(".error").length > 0){
	 $(".error:first").closest("select").focus();
	 $(".error:first").closest("textarea").focus();
	 $(".error:first").prev().focus();
} else {
	 $(".focusfield").focus();
}