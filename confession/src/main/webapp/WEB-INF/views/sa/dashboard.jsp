<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
 
<!DOCTYPE html>
<html class="no-js before-run" lang="en">
<head>
   <jsp:include page="common/innerheadimport.jsp" />
</head>
<body class="dashboard">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <jsp:include page="common/header.jsp"></jsp:include>
  
    <jsp:include page="common/menu.jsp"/>

  
  

  <!-- Page -->
  <div class="page">
    
    
    
    <div class="page-content container-fluid">
     
    <hr><hr>
     <div class="row">
            
            <div class="col-md-3">
              <div class="widget">
                <div class="widget-content widget-radius padding-25 bg-purple-600">
                  <div class="counter counter-lg counter-inverse">
                    <div class="counter-label text-uppercase">Users</div>
                    <div class="counter-number-group">
                      <span class="counter-number">2</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-md-3">
              <div class="widget">
                <div class="widget-content widget-radius padding-25 bg-white">
                  <div class="counter counter-lg">
                    <span class="counter-number">12</span>
                    <div class="counter-label text-uppercase">Email Providers</div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-md-3">
              <div class="widget">
                <div class="widget-content widget-radius padding-25 bg-blue-600">
                  <div class="counter counter-lg counter-inverse">
                    <div class="counter-label text-uppercase">Posts</div>
                    <span class="counter-number">220</span>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-md-3">
              <div class="widget">
                <div class="widget-content widget-radius padding-25 bg-white">
                  <div class="counter counter-lg">
                    <div class="counter-number-group">
                      <span class="counter-number">120</span>
                    </div>
                    <div class="counter-label text-uppercase">Emails</div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
          
         
          
          
          
    </div>
  </div>
  <!-- End Page -->


  <!-- Footer -->
  <jsp:include page="common/footer.jsp"></jsp:include>

  <jsp:include page="common/innerbottomimport.jsp" />

  
  <script>
    $(document).ready(function($) {
      Site.run();
      $("#saDashboard").addClass("active");
 
   
   
    });
  </script>
  
  
</body>

</html>