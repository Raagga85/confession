<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
 
 
  <%-- <jsp:include page="../../common/favicon.jsp" /> --%>

  <!-- Stylesheets -->
  <link rel="stylesheet" href="${context}/resources/app/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="${context}/resources/app/assets/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="${context}/resources/app/assets/css/site.min.css">

  <link rel="stylesheet" href="${context}/resources/app/assets/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="${context}/resources/app/assets/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="${context}/resources/app/assets/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="${context}/resources/app/assets/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="${context}/resources/app/assets/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="${context}/resources/app/assets/vendor/flag-icon-css/flag-icon.css">

  <!-- Plugin -->
  <link rel="stylesheet" href="${context}/resources/app/assets/vendor/chartist-js/chartist.css">
  <link rel="stylesheet" href="${context}/resources/app/assets/vendor/aspieprogress/asPieProgress.css">

  <!-- Page -->
  <link rel="stylesheet" href="${context}/resources/app/assets/css/dashboard/v2.css">

  <!-- Fonts -->
  <link rel="stylesheet" href="${context}/resources/app/assets/fonts/web-icons/web-icons.min.css">
  <link rel="stylesheet" href="${context}/resources/app/assets/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

  <!--[if lt IE 9]>
    <script src="${context}/resources/app/assets/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="${context}/resources/app/assets/vendor/media-match/media.match.min.js"></script>
    <script src="${context}/resources/app/assets/vendor/respond/respond.min.js"></script>
    <![endif]-->

  <!-- Scripts -->
  <script src="${context}/resources/app/assets/vendor/modernizr/modernizr.js"></script>
  <script src="${context}/resources/app/assets/vendor/breakpoints/breakpoints.js"></script>
  <link rel="stylesheet" href="${context}/resources/app/assets/bootstrap-select/bootstrap-select.min.css">
    
  <script>
    Breakpoints();
  </script>