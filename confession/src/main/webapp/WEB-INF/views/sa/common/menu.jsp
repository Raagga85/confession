<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%> 

<style>
.nav-side-menu {
  overflow: auto;
  font-family: verdana;
  font-size: 12px;
  font-weight: 200;
  background-color: #2e353d;
  position: fixed;
  top: 0px;
  width: 300px;
  height: 100%;
  color: #e1ffff;
}
.nav-side-menu .brand {
  background-color: #23282e;
  line-height: 50px;
  display: block;
  text-align: center;
  font-size: 14px;
}
.nav-side-menu .toggle-btn {
  display: none;
}
.nav-side-menu ul,
.nav-side-menu li {
  list-style: none;
  padding: 0px;
  margin: 0px;
  line-height: 35px;
  cursor: pointer;
  /*    
    .collapsed{
       .arrow:before{
                 font-family: FontAwesome;
                 content: "\f053";
                 display: inline-block;
                 padding-left:10px;
                 padding-right: 10px;
                 vertical-align: middle;
                 float:right;
            }
     }
*/
}
.nav-side-menu ul :not(collapsed) .arrow:before,
.nav-side-menu li :not(collapsed) .arrow:before {
  font-family: FontAwesome;
  content: "\f078";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
  float: right;
}
.nav-side-menu ul .active,
.nav-side-menu li .active {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
}
.nav-side-menu ul .sub-menu li.active,
.nav-side-menu li .sub-menu li.active {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li.active a,
.nav-side-menu li .sub-menu li.active a {
  color: #d19b3d;
}
.nav-side-menu ul .sub-menu li,
.nav-side-menu li .sub-menu li {
  background-color: #181c20;
  border: none;
  line-height: 28px;
  border-bottom: 1px solid #23282e;
  margin-left: 0px;
}
.nav-side-menu ul .sub-menu li:hover,
.nav-side-menu li .sub-menu li:hover {
  background-color: #020203;
}
.nav-side-menu ul .sub-menu li:before,
.nav-side-menu li .sub-menu li:before {
  font-family: FontAwesome;
  content: "\f105";
  display: inline-block;
  padding-left: 10px;
  padding-right: 10px;
  vertical-align: middle;
}
.nav-side-menu li {
  padding-left: 0px;
  border-left: 3px solid #2e353d;
  border-bottom: 1px solid #23282e;
}
.nav-side-menu li a {
  text-decoration: none;
  color: #e1ffff;
}
.nav-side-menu li a i {
  padding-left: 10px;
  width: 20px;
  padding-right: 20px;
}
.nav-side-menu li:hover {
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
  -webkit-transition: all 1s ease;
  -moz-transition: all 1s ease;
  -o-transition: all 1s ease;
  -ms-transition: all 1s ease;
  transition: all 1s ease;
}
@media (max-width: 767px) {
  .nav-side-menu {
    position: relative;
    width: 100%;
    margin-bottom: 10px;
  }
  .nav-side-menu .toggle-btn {
    display: block;
    cursor: pointer;
    position: absolute;
    right: 10px;
    top: 10px;
    z-index: 10 !important;
    padding: 3px;
    background-color: #ffffff;
    color: #000;
    width: 40px;
    text-align: center;
  }
  .brand {
    text-align: left !important;
    font-size: 22px;
    padding-left: 20px;
    line-height: 50px !important;
  }
}
@media (min-width: 767px) {
  .nav-side-menu .menu-list .menu-content {
    display: block;
  }
}
body {
  margin: 0px;
  padding: 0px;
}

</style>
 
   <div class="site-menubar">
  
     <div class="site-menubar-body">
      <div>
        <div>
        
         <sec:authorize access="hasAnyRole('ROLE_USER','ROLE_GOLDUSER')">
          <ul class="site-menu">
            <li class="site-menu-category">MENU</li>
           
           
            <li class="site-menu-item" id="uDashboard">
              <a class="animsition-link" href="${context}/u/dashboard">
                <i class="site-menu-icon wb-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Dashboard</span>
              </a>
            </li>
            
            <li class="site-menu-item" id="composeSMS">
              <a class="animsition-link" href="${context}/u/sendSMS">
                <i class="site-menu-icon wb-mobile" aria-hidden="true"></i>
                <span class="site-menu-title">Users</span>
              </a>
            </li>
            
             <li class="site-menu-item" id="composeEmail">
              <a class="animsition-link" href="${context}/u/sendEmail">
                <i class="site-menu-icon wb-envelope-open" aria-hidden="true"></i>
                <span class="site-menu-title">Compose Email</span>
              </a>
            </li> 
           <!--  <li class="site-menu-item" id="composeEmail">
              <a class="animsition-link" href="#">
                <i class="site-menu-icon wb-envelope-open" aria-hidden="true"></i>
                <span class="site-menu-title">Signature</span>
              </a>
            </li>  -->
            
            
            
           <%--  <li class="site-menu-item">
              <a class="animsition-link" href="${context}/u/dashboard">
                <i class="site-menu-icon wb-user" aria-hidden="true"></i>
                <span class="site-menu-title">Contacts</span>
              </a>
            </li>
          
            <li class="site-menu-item">
              <a class="animsition-link" href="${context}/u/dashboard">
                <i class="site-menu-icon wb-users" aria-hidden="true"></i>
                <span class="site-menu-title">Groups</span>
              </a>
            </li>
            
          
            
             <li class="site-menu-item">
              <a class="animsition-link" href="${context}/u/dashboard">
                <i class="site-menu-icon wb-layout" aria-hidden="true"></i>
                <span class="site-menu-title">Templates</span>
              </a>
            </li>
             --%>
            
             <li class="site-menu-item has-sub" id="settingParentID">
              <a href="javascript:void(0)">
                <i class="site-menu-icon wb-settings" aria-hidden="true"></i>
                <span class="site-menu-title">Settings</span>
              </a>
              <ul class="site-menu-sub">
               
                <%-- <li class="site-menu-item" id="settingSMSID">
                  <a class="animsition-link" href="${context}/u/settingSMS">
                    <i class="site-menu-icon " aria-hidden="true"></i>
                    <span class="site-menu-title"><i class="site-menu-icon wb-mobile" aria-hidden="true"></i> SMS Setting</span>
                  </a>
                </li> --%>
                
                <li class="site-menu-item" id="settingMAILID">
                  <a class="animsition-link" href="${context}/u/settingMAIL">
                    <i class="site-menu-icon " aria-hidden="true"></i>
                    <span class="site-menu-title"><i class="site-menu-icon wb-envelope-open" aria-hidden="true"></i> Mail Setting</span>
                  </a>
                </li>
                
            </ul>
            </li>
            
            <li class="site-menu-item" id="profileIdM">
              <a class="animsition-link" href="${context}/u/profile">
                <i class="site-menu-icon icon wb-user" aria-hidden="true"></i>
                <span class="site-menu-title">Profile</span>
              </a>
            </li>
            <sec:authorize access="hasRole('ROLE_GOLDUSER')">
            <li class="site-menu-item" id="gucontacts">
              <a class="animsition-link" href="${context}/u/contactsList">
                <i class="site-menu-icon icon wb-user" aria-hidden="true"></i>
                <span class="site-menu-title">Contacts</span>
              </a>
            </li>
            <li class="site-menu-item has-sub" id="guproducts">
              <a href="javascript:void(0)">
                <i class="site-menu-icon icon wb-user" aria-hidden="true"></i>
                <span class="site-menu-title">Product</span>
              </a>
              <ul class="site-menu-sub">
               
                <li class="site-menu-item" id="guproduct">
                  <a class="animsition-link" href="${context}/gu/productNameList">
                    <i class="site-menu-icon " aria-hidden="true"></i>
                    <span class="site-menu-title"><i class="site-menu-icon wb-mobile" aria-hidden="true"></i>Add/Update Product</span>
                  </a>
                </li> 
            </ul>
            <ul class="site-menu-sub">
               
                <li class="site-menu-item" id="guinventory">
                  <a class="animsition-link" href="${context}/gu/inventoryList">
                    <i class="site-menu-icon " aria-hidden="true"></i>
                    <span class="site-menu-title"><i class="site-menu-icon wb-mobile" aria-hidden="true"></i>Sale Purchase</span>
                  </a>
                </li> 
            </ul>
            
            </li>
            </sec:authorize>
           
            
          </ul>
</sec:authorize>




   <sec:authorize access="hasRole('ROLE_SUPER_ADMIN')">
          <ul class="site-menu">
            <li class="site-menu-category">MENU</li>
           
           
            <li class="site-menu-item" id="saDashboard">
              <a class="animsition-link" href="${context}/sa/dashboard">
                <i class="site-menu-icon wb-dashboard" aria-hidden="true"></i>
                <span class="site-menu-title">Dashboard</span>
              </a>
            </li>
            
           
           <li class="site-menu-item" id="saUserCU">
              <a class="animsition-link" href="${context}/sa/userCU?action=c">
                <i class="site-menu-icon wb-envelope" aria-hidden="true"></i>
                <span class="site-menu-title">Add User</span>
              </a>
            </li>
            
            <li class="site-menu-item" id="saManageUsers">
              <a class="animsition-link" href="${context}/sa/listUser">
                <i class="site-menu-icon wb-users" aria-hidden="true"></i>
                <span class="site-menu-title">Users List</span>
              </a>
            </li></ul>
            
</sec:authorize>




        </div>
      </div>
    </div>
    
    
  </div>
  
  
   