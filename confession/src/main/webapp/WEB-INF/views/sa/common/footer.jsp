<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
 
 
 <footer class="site-footer">
    <span class="site-footer-legal">� 2015 MailoSMS</span>
    <div class="site-footer-right">
      A Product of <a href="http://aasaanservices.in" target="blank">Aasaan Services</a>
    </div>
  </footer>